import React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import VKConverter from './Components/VKConverter.jsx';

const App = () => {

	return (
		  <VKConverter />
	);
}

export default App;

