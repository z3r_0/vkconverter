import React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {latest, allCurencyCodes} from '../api/api.js';
import {FormLayout, Select, Input, FormLayoutGroup, Button, List, Cell, InfoRow} from '@vkontakte/vkui'


class ConverterForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            currencyValue: 0,
            computedValue: 0,
            currencySelectValue: null,
            computedSelectValue : null,
            currencySelectValueList: {},
            currencyRatesData: {},
            history: [],
            veryFunnyJokeLol: ' '
        }

        this.changeCurrencyInput = this.changeCurrencyInput.bind(this);
        this.changeComputedInput = this.changeComputedInput.bind(this);
        this.changeCurrencySelect = this.changeCurrencySelect.bind(this);
        this.changeComputedSelect = this.changeComputedSelect.bind(this);
        this.onButtonClick = this.onButtonClick.bind(this);
    }

    changeCurrencyInput(e) {
        //eslint-disable-next-line
        this.state.currencyValue = e.target.value;

        if(this.state.computedSelectValue === 'Вес твоей мамки'){
            this.setState({computedValue: 999999999999999})
            return
            //Не обижайся :*
        }

        if((this.state.currencySelectValue != null) && (this.state.computedSelectValue !=null)){
            let value = this.state.currencyValue * this.state.currencyRatesData.conversion_rates[this.state.computedSelectValue];
            this.setState({computedValue: value.toFixed(2)});
        }
    }

    changeComputedInput(e) {
        if(this.state.computedSelectValue === 'Вес твоей мамки'){
            this.setState({computedValue: 999999999999999})
            return
            //Не злись :)
        }
        //eslint-disable-next-line
        this.state.computedValue = e.target.value;

        if((this.state.currencySelectValue != null) && (this.state.computedSelectValue !=null)){
            let value = this.state.computedValue / this.state.currencyRatesData.conversion_rates[this.state.computedSelectValue];
            this.setState({currencyValue: value.toFixed(2)});
        }
    }

    async changeCurrencySelect(e) {
        if(this.state.computedSelectValue === 'Вес твоей мамки'){
            this.setState({computedValue: 999999999999999})
            return
            //Купил ей скакалку 26 декабря 2004
        }

        this.setState({currencySelectValue: e.target.value});
        let result = await latest(e.target.value);
        this.setState({currencyRatesData: result});

        if(this.state.computedSelectValue != null){
            let value = this.state.currencyValue * this.state.currencyRatesData.conversion_rates[this.state.computedSelectValue];
            this.setState({computedValue: value.toFixed(2)});
        }
    }

    changeComputedSelect(e) {
        //eslint-disable-next-line
        this.state.computedSelectValue = e.target.value;
        if(e.target.value === 'Вес твоей мамки'){
            this.setState({computedValue: 999999999999999,
                            veryFunnyJokeLol: 'Вес твоей мамки(в тоннах)'})
            return
            //11 марта 2011 пошёл с ней на море, мы прыгали в воду с тарзанки
        }

        if(this.state.currencySelectValue != null){
            let value = this.state.currencyValue * this.state.currencyRatesData.conversion_rates[this.state.computedSelectValue];
            this.setState({computedValue: value.toFixed(2)});
        }
    }

    onButtonClick() {
        if((this.state.currencyValue !== 0) && (this.state.computedValue !== 0) &&
        this.state.currencySelectValue !== null && (this.state.computedSelectValue !== null)){
            this.state.history.push({
                input: this.state.currencyValue + ' ' + this.state.currencySelectValue,
                output: this.state.computedValue + ' ' + this.state.computedSelectValue
            })
            this.setState({computedValue: this.state.computedValue}) //Чтоб вызвать рэндер :D
        }
    }

    async componentWillMount() {
        let currencyData = allCurencyCodes();
        this.setState({currencySelectValueList: currencyData});
    }


    render() {
        return(
            <FormLayoutGroup>
                <FormLayout>
                    <Select top="Конвертировать из:" placeholder="Выберите валюту"  
                    onChange={this.changeCurrencySelect}>
                        {this.state.currencySelectValueList.map(currency => <option key={currency} value={currency}>{currency}</option>)}
                    </Select>
                    <FormLayoutGroup top="Количество">
                      <Input type="number" onChange={this.changeCurrencyInput} value={this.state.currencyValue} min={0} />
                    </FormLayoutGroup>
                </FormLayout>
                <FormLayout>
                    <Select top="Конвертировать в:" placeholder="Выберите валюту" 
                    onChange={this.changeComputedSelect} value={this.state.computedSelectValue}>
                    {this.state.currencySelectValueList.map(currency => <option key={currency} value={currency}>{currency}</option>)}
                    <option key="Вес твоей мамки" value="Вес твоей мамки">{this.state.veryFunnyJokeLol}</option>{/* Если не жирная, тогда ладно :( */}
                    </Select>
                    <FormLayoutGroup top="Результат">
                      <Input type="number" onChange={this.changeComputedInput} value={this.state.computedValue} min={0} />
                    </FormLayoutGroup>
                </FormLayout>
                <FormLayout>
                    <Button size="xl" mode="primary" onClick={this.onButtonClick}>Сохранить</Button>
                </FormLayout>
                <FormLayout>
                    <List align="center">
                        {this.state.history.map((item) => <Cell><InfoRow title>{item.input + ' = ' + item.output}</InfoRow></Cell>)}
                    </List>
                </FormLayout>
            </FormLayoutGroup>
        )
    }
}

export default ConverterForm;