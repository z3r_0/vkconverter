import React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import ConverterForm from './Converter';
import About from './About';
import {View, Panel, PanelHeader, Tabbar, TabbarItem, Epic} from '@vkontakte/vkui'
import Icon28MoneyCircleOutline from '@vkontakte/icons/dist/28/money_circle_outline';
import Icon28InfoOutline from '@vkontakte/icons/dist/28/info_outline';

export default class VKConverter extends React.Component {
	constructor (props) {
	  super(props);
	 
	  this.state = {
		activeStory: 'converter'
	  };
	  this.onStoryChange = this.onStoryChange.bind(this);
	}
	 
	onStoryChange (e) {
	  this.setState({ activeStory: e.currentTarget.dataset.story })
	}
  
	render() {
	  return (
		<Epic activeStory={this.state.activeStory} tabbar={
			<Tabbar>
			  <TabbarItem
				onClick={this.onStoryChange}
				selected={this.state.activeStory === 'converter'}
				data-story="converter"
				text="Конвертер"
			  ><Icon28MoneyCircleOutline /></TabbarItem>
			  <TabbarItem
				onClick={this.onStoryChange}
				selected={this.state.activeStory === 'about'}
				data-story="about"
				text="Об авторе"
			  ><Icon28InfoOutline/></TabbarItem>
			</Tabbar>
		  }>
			<View id="converter" activePanel="converter">
			  <Panel id="converter">
				<PanelHeader>Конвертер</PanelHeader>
                <ConverterForm></ConverterForm>
			  </Panel>
			</View>
			<View id="about" activePanel="about">
			  <Panel id="about">
				<PanelHeader>Об авторе</PanelHeader>
		  		<About />
			  </Panel>
			</View>
		  </Epic>
	  	)
	}
}