import React from 'react';
import RichCell from '@vkontakte/vkui/dist/components/Cell/Cell';
import bridge from '@vkontakte/vk-bridge';
import {Group, Avatar, Button, InfoRow} from '@vkontakte/vkui'

class About extends React.Component{

    onClickSubscribeButton(){
        bridge.send("VKWebAppJoinGroup", {"group_id": 199134322});
    }

    onClickRepostButton(){
        bridge.send("VKWebAppShowWallPostBox", {"message": "Мое тестовое VK Mini Apps приложение: https://vk.com/app7611813_237964467"});
    }
    

    render(){
        return(
            <Group>
                <InfoRow header="Разработчик этого костыльного шедевра" align="center"></InfoRow>
                <RichCell align="center">
                    <Avatar align="center" size={100} src='https://sun9-62.userapi.com/0erN_focjVZKevvEvICqO_qVnCQP3lqEPbnfow/bHh2dYiecuM.jpg' />
                    <h1>Илья Герасимов</h1>
                </RichCell>
                <InfoRow header="Обо мне" align="center"></InfoRow>
                <p align="center">
                    Привет, я студент с тремя всевышними образованиями,
                    мне так сказали мои воображаемые друзья.
                    Чтобы написать этот шЫдевр, мне пришлось выучить счёт древних русов.
                </p>
                <Group>
                    <Button size="xl" mode="secondary" onClick={this.onClickSubscribeButton}>Подписаться на сообщество</Button>
                </Group>
                <Group>
                    <Button size="xl" mode="secondary" onClick={this.onClickRepostButton}>Репост</Button>
                </Group>
            </Group>
        );
    }
}

export default About;