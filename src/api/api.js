import axios from 'axios';

const ApiKey = 'a6a0968b0ce185e620cbc701';
const CorsHost = `https://cors-anywhere.herokuapp.com/`;

const api = axios.create({
    baseURL: CorsHost + `https://v6.exchangerate-api.com/v6/${ApiKey}/`,
    timeout: 1000*10,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': '/',
        'Access-Control-Allow-Origin': '*'
    }
});

export async function latest(currencyCode){
    let result = await api.get(`latest/${currencyCode}`);
    if(result !== null)
        return (await api.get(`latest/${currencyCode}`)).data;
}

export function allCurencyCodes(){
    return [
        'RUB', 'AED', 'ARS', 'AUD', 'BGN', 'BRL',
        'BSD', 'CAD', 'CHF', 'CLP', 'CNY', 'COP',
        'CZK', 'DKK', 'DOP', 'EGP', 'EUR', 'FJD',
        'GBP', 'GTQ', 'HKD', 'HRK', 'HUF', 'IDR',
        'ILS', 'INR', 'ISK', 'JPY', 'KRW', 'KZT',
        'MVR', 'MXN', 'MYR', 'NOK', 'NZD', 'PAB',
        'PEN', 'PHP', 'PKR', 'PLN', 'PYG', 'RON',
        'SAR', 'SEK', 'SGD', 'THB', 'TRY', 'TWD',
        'UAH', 'USD', 'UYU', 'ZAR'
      ];
};